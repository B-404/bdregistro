
package root.heroku.apirestbdpersonas;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.heroku.apirestbdpersonas.dao.BdRegistroJpaController;
import root.heroku.apirestbdpersonas.dao.exceptions.NonexistentEntityException;
import root.heroku.apirestbdpersonas.entity.BdRegistro;




@Path("registro")
public class BdRegistroPersonas {
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarRegistroPersonas(){
        
        BdRegistroJpaController dao = new BdRegistroJpaController();
        List<BdRegistro>lista=dao.findBdRegistroEntities();
        
        
        return Response.ok(200).entity(lista).build();
        
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(BdRegistro bdRegistro){
    
        try {
            BdRegistroJpaController dao = new BdRegistroJpaController();
            dao.create(bdRegistro);
        } catch (Exception ex) {
            Logger.getLogger(BdRegistroPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        return Response.ok(200).entity(bdRegistro).build();
    }
    
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("ideliminar")String ideliminar){
    
        try {
            BdRegistroJpaController dao = new BdRegistroJpaController();
            
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BdRegistroPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Ingreso Eliminado").build();
    }
    
    
    @PUT
    public Response update(BdRegistro BdRegistro){
    
        try {
            BdRegistroJpaController dao = new BdRegistroJpaController();
            dao.edit(BdRegistro);
        } catch (Exception ex) {
            Logger.getLogger(BdRegistroPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
    
         return Response.ok(200).entity(BdRegistro).build();
    }   
}
