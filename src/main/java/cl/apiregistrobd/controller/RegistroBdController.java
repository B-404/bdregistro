/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.apiregistrobd.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import root.heroku.apirestbdpersonas.entity.BdRegistro;

/**
 *
 * @author ELIAS
 */
@WebServlet(name = "RegistroBdController", urlPatterns = {"/RegistroBdController"})
public class RegistroBdController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistroBdController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistroBdController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        
        if (accion.equals("entrada")){
            request.getRequestDispatcher("registro.jsp").forward(request, response);
           
            
            
        }
        
        if (accion.equals("inscribir")){
             BdRegistro bdregistro=new BdRegistro();
            bdregistro.setNombre(request.getParameter("nombre"));
            bdregistro.setRut(request.getParameter("rut"));
            bdregistro.setEmail(request.getParameter("email"));
            bdregistro.setDireccion(request.getParameter("direccion"));
            Client client = ClientBuilder.newClient();
            WebTarget myResource1=client.target(" http://DESKTOP-TR5OGRJ:8080/ApirestBdPersonas-1.0-SNAPSHOT/api/registro");
           BdRegistro bdRegistro1= myResource1.request(MediaType.APPLICATION_JSON).post(Entity.json(bdregistro),BdRegistro.class);
            
        }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
